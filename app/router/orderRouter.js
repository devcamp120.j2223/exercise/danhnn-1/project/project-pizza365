// khởi tạo bộ thư viện
const express = require('express');

const { createOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById } = require('../controller/orderController');

//khởi tạo router
const router = express.Router();

router.get("/users/:userid/orders", getAllOrder)

router.post("/users/:userid/orders", createOrder)

router.get("/orders/:orderId", getOrderById)

router.put("/orders/:orderId", updateOrderById)

router.delete("/users/:userid/orders/:orderId", deleteOrderById )

// export dữ liệu thành 1 module
module.exports = router;