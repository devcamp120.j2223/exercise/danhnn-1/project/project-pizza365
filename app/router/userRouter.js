// khởi tạo bộ thư viện
const express = require('express');
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require('../controller/userController');

//khởi tạo router
const router = express.Router();

router.get("/user", getAllUser)

router.post("/user", createUser)

router.get("/user/:userId", getUserById)

router.put("/user/:userId", updateUserById)

router.delete("/user/:userId", deleteUserById)

// export dữ liệu thành 1 module
module.exports = router;