// import order model
const orderModel = require('../model/orderModel');
const userModel = require('../model/userModel');
const { localStorage } = require("node-localstorage");

// import mongoose
const mongoose = require('mongoose');

const createOrder = (request, response) => {
    //b1: chuẩn bị dữ liệu
    let userid = request.params.userid;

    let requestBody = request.body;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "User ID is invalid"
        })
    }

    if (!requestBody.orderCode) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "orderCode is invalid"
        })
    }

    if (!requestBody.pizzaSize) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "pizzaSize is invalid"
        })
    }

    if (!requestBody.pizzaType) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "pizzaType is invalid"
        })
    }

    if (!requestBody.status) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "status is invalid"
        })
    }

    // b3: thao tác với cơ sở dữ liệu
    let createOrder = {
        _id: mongoose.Types.ObjectId(),
        orderCode: requestBody.orderCode,
        pizzaSize: requestBody.pizzaSize,
        pizzaType: requestBody.pizzaType,
        drink: { voucherId: requestBody.drink },
        voucher: { drinkId: requestBody.voucher },
        status: requestBody.status,
    }

    orderModel.create(createOrder, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            userModel.findByIdAndUpdate(userid, {
                $push: { orders: data._id }
            },
                (err, updateUser) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal sever error",
                            message: err.message
                        })
                    } else {
                        return response.status(201).json({
                            status: "Create Order Success",
                            data: data
                        })
                    }
                })
        }
    })
}

const getAllOrder = (request, response) => {
    // bước 1: thu thập dữ liệu
    let userid = request.params.userid;
    // bước 2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "User Id is invalid"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu
    userModel.findById(userid)
        .populate('orders')
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal Error Sever",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get All order Success",
                    data: data.orders
                })
            }
        })
}

const getOrderById = (request, response) => {
    // bước 1: thu thập dữ liệu
    let orderId = request.params.orderId;
    // bước 2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Order Id is invalid"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: err.message
            })
        } else {
            return response.status(200).json({
                status: "Get Order By Id Success",
                data: data
            })
        }
    })
}

const updateOrderById = (request, response) => {
    // bước 1: thu thập dữ liệu
    let orderId = request.params.orderId;
    let orderBody = request.body;

    // bước 2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Order Id is invalid"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu

    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: err.message
            })
        } else {
            const save_drink = data.drink.drinkId;
            const save_voucher = data.voucher.voucherId;

            let updateOrder = {
                pizzaSize: orderBody.pizzaSize,
                pizzaType: orderBody.pizzaType,
                drink: { drinkId: orderBody.drinkId ? orderBody.drinkId : save_drink },
                voucher: { voucherId: orderBody.voucherId ? orderBody.voucherId : save_voucher},
                status: orderBody.status,
            }

            orderModel.findByIdAndUpdate(orderId, updateOrder, (error, data) => {
                if (error) {
                    return response.status(500).json({
                        status: "Error 500: Internal sever error",
                        message: error.message
                    })
                } else {
                    return response.status(200).json({
                        status: "Update Order By Id Success",
                        data: data
                    })
                }
            })
        }
    })
}

const deleteOrderById = (request, response) => {
    // bước 1: thu thập dữ liệu
    let orderId = request.params.orderId;
    let userid = request.params.userid;
    // bước 2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Order Id is invalid"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "User Id is invalid"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu
    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: err.message
            })
        } else {
            userModel.findByIdAndUpdate(userid, {
                $pull: { orders: orderId }
            },
                (err, updateUser) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal sever error",
                            message: err.message
                        })
                    } else {
                        return response.status(204).json({
                            status: "Delete Order success"
                        })
                    }
                }
            )
        }
    })
}

// export thành 1 module
module.exports = {
    createOrder: createOrder,
    getAllOrder: getAllOrder,
    getOrderById: getOrderById,
    updateOrderById: updateOrderById,
    deleteOrderById: deleteOrderById
}
