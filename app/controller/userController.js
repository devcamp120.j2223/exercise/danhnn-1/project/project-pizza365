// import user model
const userModel = require('../model/userModel');

// import mongoose
const mongoose = require('mongoose');

const createUser = (request, response) => {
    // bước 1: thu thập dữ liệu
    let bodyUser = request.body;
    // bước 2: kiểm tra dữ liệu
    if (!bodyUser.fullName) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "fullName is require"
        })
    }

    if (!bodyUser.email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "email is require"
        })
    }

    if (!bodyUser.address) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "address is require"
        })
    }

    if (!bodyUser.phone) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "phone is require"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu
    let createUser = {
        _id: mongoose.Types.ObjectId(),
        fullName: bodyUser.fullName,
        email: bodyUser.email,
        address: bodyUser.address,
        phone: bodyUser.phone
    }
    userModel.create(createUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal Error Sever",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Create User Success",
                data: data
            })
        }
    })

}
const getAllUser = (request, response) => {
    // bước 1: thu thập dữ liệu    
    // bước 2: kiểm tra dữ liệu
    // bước 3: thao tác với cơ sở dữ liệu
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal Error Sever",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get All User Success",
                data: data
            })
        }
    })
}
const getUserById = (request, response) => {
    // bước 1: thu thập dữ liệu
    let userId = request.params.userId
    // bước 2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is require"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu
    userModel.findById(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal Error Sever",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get User By Id Success",
                data: data
            })
        }
    })
}
const updateUserById = (request, response) => {
    // bước 1: thu thập dữ liệu
    let userId = request.params.userId
    let userBody = request.body;
    // bước 2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is require"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu
    let updateUser = {
        fullName: userBody.fullName,
        email: userBody.email,
        address: userBody.address,
        phone: userBody.phone
    }
    userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal Error Sever",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Update User By Id Success",
                data: data
            })
        }
    })
}
const deleteUserById = (request, response) => {
    // bước 1: thu thập dữ liệu
    let userId = request.params.userId
    // bước 2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is require"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal Error Sever",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Delete User By Id Success",
            })
        }
    })
}

// export thành 1 module
module.exports = {
    createUser: createUser,
    getAllUser: getAllUser,
    getUserById: getUserById,
    updateUserById: updateUserById,
    deleteUserById: deleteUserById
}